class User {
    constructor(name) {
        this.name = name;

        this.logName();
    }

    logName() {
        console.log(this.name);
    }
}

export default User;
