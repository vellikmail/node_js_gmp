class Product {
    constructor(type) {
        this.type = type;

        this.logType();
    }

    logType() {
        console.log(this.type);
    }
}

export default Product;
